#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import re
import os
import sys
import json
import gzip
import argparse

from collections import defaultdict


def infinite_defaultdict():
    return defaultdict(infinite_defaultdict)


ACCESS_LINE_REGEX = re.compile(
        r'(?P<ip>[0-9]{1,3}(\.[0-9]{1,3}){3})'
        r' - - '
        r'\[(?P<date>[0-9]{4}-[01][0-9]\-[0-3][0-9]) [0-9:\+]+]'
        r' '
        r'"(?P<http_req>.*)"'
        r' '
        r'(?P<http_status>[0-9]{3})'
        r' [0-9]+ "-" '
        r'"(?P<user_agent>.*)"'
        r'.*')


HTTP_GOOD_REQ_LINE = re.compile(
        r'(?P<method>(GET|HEAD|OPTIONS|POST))'
        r' '
        r'(?P<path>/.*)'
        r' '
        r'HTTP/(?P<ver>[0-9\.]+)')


APK_PATH = re.compile(
        r'/repo/(?P<pkg>[a-zA-Z0-9\._]+)_(?P<ver>[0-9]+)(_[0-9a-f]*)?.apk')


def init_or_increment_counter(d, key):
    if key in d:
        d[key] += 1
    else:
        d[key] = 1


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('logfile', help="logfiles to process")
    parser.add_argument('--pretty', '-p', action='store_true',
                        default=False, help="pretty print")
    args = parser.parse_args()

    if not os.path.isfile(args.logfile):
        print("error: '{}' is not a file".format(args.logfile))
        sys.exit(1)

    stat = infinite_defaultdict()
    with gzip.open(args.logfile, 'rt', encoding='utf-8') as f:
        for line in f:
            m = ACCESS_LINE_REGEX.match(line)
            if not m:
                print('error parsing line: ' + line)
                sys.exit(1)

            init_or_increment_counter(stat['http_status_counts'],
                                      m.group('http_status'))

            if m.group('http_status' == '200'):
                rm = HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
                if rm and rm.group('method') == 'GET':
                    am = APK_PATH.match(rm.group('path'))
                    if am:
                        init_or_increment_counter(stat['apk_downloads'],
                                                  am.group('pkg'))

    if args.pretty:
        print(json.dumps(stat, indent=4, sort_keys=True))
    else:
        print(json.dumps(stat))
