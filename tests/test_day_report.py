# SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import day_report


lines = ['5.9.48.82 - - [2019-08-13 00:00:00+00:00] "GET '
         '/archive/org.fdroid.fdroid/en-US/phoneScreenshots/1.png '
         'HTTP/1.1" 404 13214 "-" "F-Droid/1.7 (Android)"',

         '5.9.48.82 - - [2025-01-01 00:00:00+00:00] "HEAD '
         '/archive/index-v1.jar '
         'HTTP/1.1" 200 0 "-" "F-Droid/1.7 (Android)"',

         '148.251.140.42 - - [2019-08-13 04:00:00+00:00] "POST '
         '/en/contribute/trackback/ '
         'HTTP/1.1" 404 18498 "-" "IE/11.0 (Windows 7)"',

         '148.251.140.42 - - [2019-08-13 06:00:00+00:00] "OPTIONS '
         '/ HTTP/1.1" 200 843 "-" "Chrome/67.0.3396 (Linux )"',

         '0.0.0.0 - - [2019-08-13 09:00:00+00:00] "\\n" '
         '400 5793 "-" "Other/ (Other )"']


def test_regexes_line_1():
    m = day_report.ACCESS_LINE_REGEX.match(lines[0])
    assert m is not None
    assert m.group('ip') == '5.9.48.82'
    assert m.group('date') == '2019-08-13'
    rm = day_report.HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
    assert rm is not None
    assert rm.group('method') == 'GET'
    assert rm.group('path') == \
        '/archive/org.fdroid.fdroid/en-US/phoneScreenshots/1.png'
    assert rm.group('ver') == '1.1'
    assert m.group('http_status') == '404'
    assert m.group('user_agent') == 'F-Droid/1.7 (Android)'


def test_regexes_line_2():
    m = day_report.ACCESS_LINE_REGEX.match(lines[1])
    assert m is not None
    assert m.group('ip') == '5.9.48.82'
    assert m.group('date') == '2025-01-01'
    rm = day_report.HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
    assert rm is not None
    assert rm.group('method') == 'HEAD'
    assert rm.group('path') == '/archive/index-v1.jar'
    assert rm.group('ver') == '1.1'
    assert m.group('http_status') == '200'
    assert m.group('user_agent') == 'F-Droid/1.7 (Android)'


def test_regexes_line_3():
    m = day_report.ACCESS_LINE_REGEX.match(lines[2])
    assert m is not None
    assert m.group('ip') == '148.251.140.42'
    assert m.group('date') == '2019-08-13'
    rm = day_report.HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
    assert rm is not None
    assert rm.group('method') == 'POST'
    assert rm.group('path') == '/en/contribute/trackback/'
    assert rm.group('ver') == '1.1'
    assert m.group('http_status') == '404'
    assert m.group('user_agent') == 'IE/11.0 (Windows 7)'


def test_regexes_line_4():
    m = day_report.ACCESS_LINE_REGEX.match(lines[3])
    assert m is not None
    assert m.group('ip') == '148.251.140.42'
    assert m.group('date') == '2019-08-13'
    rm = day_report.HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
    assert rm is not None
    assert rm.group('method') == 'OPTIONS'
    assert rm.group('path') == '/'
    assert rm.group('ver') == '1.1'
    assert m.group('http_status') == '200'
    assert m.group('user_agent') == 'Chrome/67.0.3396 (Linux )'


def test_regexes_line_5():
    m = day_report.ACCESS_LINE_REGEX.match(lines[4])
    assert m is not None
    assert m.group('ip') == '0.0.0.0'
    assert m.group('date') == '2019-08-13'
    rm = day_report.HTTP_GOOD_REQ_LINE.match(m.group('http_req'))
    assert rm is None
    assert m.group('http_status') == '400'
    assert m.group('user_agent') == 'Other/ (Other )'


def test_apk_regex():
    m = day_report.APK_PATH.match('/repo/com.nextcloud.client_30070052.apk')
    assert m is not None
    assert m.group('pkg') == 'com.nextcloud.client'
    assert m.group('ver') == '30070052'
